﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo2
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Ex 1
            // ex 1
            // Créer un tableau de 5 éléments (int)
            // et demander à l'utilisateur d'entrer ces 5 élements
            // Afficher la moyenne des entrées de l'utilisateur

            //int longueur = 5;
            //int[] tab = new int[longueur];
            //for (int i = 0; i < longueur; i++)
            //{
            //    tab[i] = int.Parse(Console.ReadLine());
            //}
            //double total = 0;
            //foreach(int item in tab)
            //{
            //    total += item;
            //}
            //Console.WriteLine(total / longueur);
            //Console.ReadKey(); 
            #endregion

            #region Ex 2
            //// ex 2
            //// Créer une liste
            //// et demander à l'utisateur d'entrer des nombres entiers
            //// et lorsque la dernière entrée n'est plus un nombre calculer la moyenne et l'afficher
            //List<int> liste = new List<int>();
            //while(int.TryParse(Console.ReadLine(), out int val))
            //{
            //    liste.Add(val);
            //}
            //double total = 0;
            //foreach(int item in liste)
            //{
            //    total += item;
            //}
            //Console.WriteLine(total / liste.Count);
            //Console.ReadKey();
            #endregion

            #region Ex 3
            // ex 3 
            // demander à l'utilisateur d'entrer 10 nombres diférents
            // Afficher le tableau trié par ordre
            //const int NB_ELEMENTS = 5;
            //int[] monTableauUser = new int[NB_ELEMENTS];
            //int cpt = 0;
            //while (cpt < NB_ELEMENTS)
            //{
            //    Console.WriteLine("Veuiller introduire en chiffre entier");

            //    int nbEntier;

            //    while (!int.TryParse(Console.ReadLine(), out nbEntier))
            //    {
            //        Console.WriteLine("Nombre incorrect");
            //    }

            //    if (!monTableauUser.Contains(nbEntier))
            //    {
            //        monTableauUser[cpt] = nbEntier;
            //        cpt++;
            //    }
            //    else
            //    {
            //        Console.WriteLine("Le nombre existe deja dans le tableau");
            //    }
            //}

            //for (int j = 0; j < monTableauUser.Length; j++)
            //{
            //    for (int i = 0; i < monTableauUser.Length; i++)
            //    {
            //        if (i < monTableauUser.Length - 1)
            //        {
            //            int currentValue = monTableauUser[i];
            //            int nextValue = monTableauUser[i + 1];

            //            if (currentValue > nextValue)
            //            {
            //                monTableauUser[i] = nextValue;
            //                monTableauUser[i + 1] = currentValue;
            //            }
            //            Console.WriteLine();
            //        }
            //    }
            //}

            //Console.WriteLine("Mon tableau trié");
            //foreach (int item in monTableauUser)
            //{
            //    Console.WriteLine(item);
            //} 
            #endregion
            Console.ReadKey();

        }
    }
}
