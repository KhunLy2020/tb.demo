﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo6
{
    class Program
    {
        static void Main(string[] args)
        {
            //collections ! références
            // tableaux // taille obligatoire
            int[] tableau = new int[10];
            tableau[5] = 42;
            // Queue // pas de taille // first in first out // pas indexable
            Queue<int> queue = new Queue<int>();
            //impossible
            //queue[0]


            queue.Enqueue(45);
            queue.Enqueue(42);
            queue.Enqueue(76);

            int nb = queue.Dequeue();
            Console.WriteLine(nb);
            nb = queue.Dequeue();
            Console.WriteLine(nb);

            // Stack pas de taille // last in first out //pas indexable
            Stack<int> stack = new Stack<int>();
            stack.Push(45);
            stack.Push(42);
            stack.Push(76);

            nb = stack.Pop();
            Console.WriteLine(nb);
            nb = stack.Pop();
            Console.WriteLine(nb);

            // List
            // pas de taille // ajout d'élément // index sur les élements // suppression
            // indexable
            List<int> liste = new List<int>();
            liste.Add(45);
            liste.Add(42);
            liste.Add(76);

            liste[1] = 55;

            Console.WriteLine(liste[0]);
            Console.WriteLine(liste[2]);

            liste.RemoveAt(1);

            Console.WriteLine(liste.Count);
            Console.WriteLine(queue.Count);

            Console.WriteLine(tableau.Length);

            int[][] tableau2 = new int[5][];
            tableau2[0] = new int[3];
            tableau2[1] = new int[6];

            int[,] tableau3 = new int[5, 6];
            Console.WriteLine(tableau3);

            tableau3[1, 1] = 45;



            Console.WriteLine("Veuillez Entrer un nombre");
            int nombre;
            while(!int.TryParse(Console.ReadLine(), out nombre))
            {
                Console.WriteLine("Entrée incorrecte");
            }
            Console.WriteLine(nombre * 2);

            // oprérateurs logiques
            // && AND
            // || OU INCLUSIF
            int a = 0;
            int b = 42;
            bool condition = a < 5 || b < 9;
            if(condition)
            {
                //faire qq chose
            }


            for(int i=0; i<9; i++)
            {
                Console.WriteLine(i);
                //instructions sup
            }
            Console.WriteLine("Valeurs du tableau");
            for (int i = 0; i < tableau.Length; i++)
            {
                Console.WriteLine(tableau[i]);
            }
            //Console.WriteLine(i);

            string[] tableau5 = new string[] { "Riri", "Fifi", "Loulou" };

            Console.WriteLine("Valeurs du tableau seconde facon");
            foreach(string item in tableau5)
            {
                Console.WriteLine(item);
            }
            

            Console.ReadKey();

        }
    }
}
