﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo5
{
    class Program
    {
        static void Main(string[] args)
        {
            SePresenter("Khun", 37);
            SePresenter("Sereno", 17);

            List<int> li = new List<int> { 1,2,3,4,5,6,7 };
            double moyenne = CalculMoyenne(li);
            Console.WriteLine("La moyenne est égale à" + moyenne);

            double moyenne2 = CalculMoyenne2(42,45,999,1000,50,70);
            Console.WriteLine("La moyenne est égale à" + moyenne2);


            List<int> listeDoublee = Doubler(li);
            Console.WriteLine("Liste Originale");
            Console.WriteLine(string.Join(",", li));
            Console.WriteLine("Nouvelle liste");
            Console.WriteLine(string.Join(",", listeDoublee));

            EleverAuCarre(li);
            Console.WriteLine("Liste Originale");
            Console.WriteLine(string.Join(",", li));

            Console.WriteLine(string.Join(" - ", "papa", "maman",  "mami"));


            Console.WriteLine(Fibo(100));

            //Console.WriteLine(RFibo(100));

            Console.ReadKey();
        }

        // ex1.1
        public static void SePresenter(string nom, int age)
        {
            Console.WriteLine($"Bonjour je m'appelle {nom} et j'ai {age} ans");
        }
        // ex1.2
        public static double CalculMoyenne(List<int> liste)
        {
            double somme = 0;
            foreach (int item in liste) somme += item;
            return somme / liste.Count;
        }

        public static double CalculMoyenne2(params int[] parameters)
        {
            double somme = 0;
            foreach (int item in parameters) somme += item;
            return somme / parameters.Length;
        }

        // ex 1.3
        public static List<int> Doubler(List<int> liste)
        {
            List<int> resultatFinal = new List<int>();
            foreach (int item in liste)
                resultatFinal.Add(item * 2);
            return resultatFinal;
        }



        // ex 1.4
        public static void EleverAuCarre(List<int> liste)
        {
            for (int i = 0; i < liste.Count; i++)
            {
                int ancienneValeur = liste[i];
                int nouvelleValeur = ancienneValeur * ancienneValeur;
                liste[i] = nouvelleValeur;
            }
        }

        // ex 1.5
        public static long Fibo(long n)
        {
            long Fn = 1;
            long Fn_1 = 0;
            long count = 1;
            //int resultat = Fn + Fn_1;
            while (n > count)
            {
                long temp = Fn_1; 
                Fn_1 = Fn;
                Fn = temp + Fn;
                //resultat = Fn + Fn_1;
                count++;
            }
            return Fn;
        }

        public static long RFibo(long n)
        {
            if (n == 0) return 0;
            if (n == 1) return 1;
            return RFibo(n - 1) + RFibo(n - 2); 
        }

        // Ex6 Solution
        // Soit f(x) = x² - n
        // => f'(x) = 2x
        // Soit a une approximation de la racine de n 
        // f'(a) = 2a (pente de la tengente en a)
        // Equation de la tangente := y - f(a) = 2a(x-a)
        // Quand y = 0 => -a² + n = 2a² - 2ax
        // la valeur de x en fonction de a et de n nous donne une nouvelle approximation plus précise
        // <=> 2ax = a² + n
        // <=> x = (a² + n) / 2a
        // <=> x = (a + n/a) / 2
        public static double Racine(int n)
        {
            double precision = 0.0000000001;
            double approximation = 1;
            while(Math.Abs(approximation*approximation - n) > precision)
            {
                approximation = (approximation + (n / approximation)) / 2;
            }
            return approximation;
        }
    }
}
