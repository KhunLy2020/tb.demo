﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Veuillez entrer 2 nombres");
            string reponse1 = Console.ReadLine();
            string reponse2 = Console.ReadLine();

            // conversion string -> entier
            int nombre1 = int.Parse(reponse1);
            int nombre2 = int.Parse(reponse2);

            // conversion entier -> string
            string valeur = nombre1.ToString();

            Console.WriteLine("le resultat est : " + (nombre1 + nombre2));
            Console.ReadKey();
        }
    }
}
