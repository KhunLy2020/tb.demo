﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo9_Fonction
{
    class Program
    {
        static void Main(string[] args)
        {
            //List<string> liste = new List<string>
            //{
            //    "a", "b", "c"
            //};

            //// Fonction sans retour et sans paramètre
            //DireBonjour();

            ////["a","b","c"]
            //// Fonction sans retour avec un paramètre
            //AfficherListe(liste);

            //// Fonction sans parametre avec retour
            //int carre = CalculerLeCarreDe111();
            //Console.WriteLine(carre);


            //// Fonction avec parametre avec retour
            //int carre2 = CalculerCarre(6);
            //Console.WriteLine(carre2);
            int nb;
            bool reussi = int.TryParse("42", out nb);
            Console.WriteLine(reussi);
            Console.WriteLine(nb);

            int a = 42;
            Console.WriteLine(a);
            //ModifierNombre(a);
            ModifierNombreRef(out a);
            Console.WriteLine(a);

            List<int> li = new List<int> { 1, 2 };
            AfficherListe(li);
            //MofierListe(li);
            // Fonction avec Paremtre par ref
            MofierListeRef(out li);
            AfficherListe(li);


            Console.ReadKey();
        }

        public static void MofierListe(List<int> li)
        {
            // modifie une copie
            li = new List<int> { 0,0 };
        }

        public static void ModifierNombre(int a)
        {
            // modifie une copie de la valeur oginale
            a = 0;
        }

        public static void MofierListeRef(out List<int> li)
        {
            // modifie la reférence de la liste
            li = new List<int> { 0, 0 };
        }

        public static void ModifierNombreRef(out int a)
        {
            // modifie la reférence de a
            a = 0;
        }

        public static int CalculerCarre(int v)
        {
            return v * v;
        }

        public static int CalculerLeCarreDe111()
        {
            return 111 * 111;
        }

        public static void DireBonjour()
        {
            Console.WriteLine("Hello World");
        }

        public static void AfficherListe(List<int> liste)
        {
            Console.WriteLine("[" + string.Join(",", liste.ToArray()) + "]");
        }
    }
}
