﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            string[,] tab = new string[3, 10];
            //List<(string, string, string)> liste = new List<(string, string, string)>();

            // créer un fichier
            // File.Create(@"\chemin\nom.txt");

            // vérifie l'existance d'un fichier
            // File.Exists("nom.txt");

            // File nécessite le using System.IO
            string[] questions = File.ReadAllLines("questions.txt");
            string[] reponses = File.ReadAllLines("reponses.txt");
            string[] corrections = File.ReadAllLines("corrections.txt");
            // Supprime toutes les lignes et remplace
            //File.WriteAllLines("path.txt", new List<string> { "line1", "line2", "line3" });
            
            // ajoute les lignes ...
            //File.AppendAllLines("path.txt", new List<string> { "line1", "line2", "line3" });

            for (int i = 0; i < questions.Length; i++)
            {
                tab[0, i] = questions[i];
                tab[1, i] = reponses[i];
                tab[2, i] = corrections[i];

                //liste.Add((questions[i], reponses[i], corrections[i]));
            }

            

            Console.ReadKey();
        }
    }
}
