﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo4
{
    class Program
    {
        static void Main(string[] args)
        {
            int age = 18;

            //age = null;
            //if (age >= 18)
            //{
            //    Console.WriteLine("Vous êtes majeur");
            //}
            //else
            //{
            //    Console.WriteLine("Vous êtes mineur");
            //}

            // entourer des instructions pas ctrl + k + s
            //Console.WriteLine("Salut");
            //Console.ReadLine();

            // si une seule instructions de le block
            //if (age >= 18)
            //    Console.WriteLine("Vous êtes majeur");
            //else
            //    Console.WriteLine("Vous êtes mineur");

            string value = age > 18 ? "Vous etes majeur" : (age < 18 ? "Vous etes mineur" : "Ne sais pas");


            Console.WriteLine(value);

            int prix = 20;
            int? promotion = null;

            //coalesce
            Console.WriteLine(prix - (prix * (promotion ?? 0) / 100));
            Console.WriteLine(prix - (prix * (promotion != null ? promotion : 0) / 100));

            // null safe operator
            string maChaine = null;

            Console.WriteLine(maChaine?.ToLower());



            Console.ReadKey();
        }
    }
}
