﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo11_ConsoleTricks
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 0;
            int y = 0;
            Console.OutputEncoding = Encoding.UTF8;
            Console.CursorVisible = false;
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Clear();

            Console.SetCursorPosition(x,y);
            Console.WriteLine("☺");

            while(true)
            {
                ConsoleKey key = Console.ReadKey().Key;
                //Console.Clear();
                Console.SetCursorPosition(x, y);
                Console.WriteLine(" ");
                if (key == ConsoleKey.UpArrow)
                    y--;
                else if (key == ConsoleKey.DownArrow)
                    y++;
                else if (key == ConsoleKey.LeftArrow)
                    x--;
                else if (key == ConsoleKey.RightArrow)
                    x++;
                Console.SetCursorPosition(x, y);
                Console.WriteLine("☺");
                
            }


            Console.ReadKey();
        }
    }
}
