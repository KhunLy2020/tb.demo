﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ConsoleUtils;

namespace exo1
{
    class Program
    {
        static void Main(string[] args)
        {
            int total = int.Parse(Utils.Question("Entrer un nombre de seconde", 50));
            int h = total / 3600;
            int m = (total % 3600) / 60;
            //int m = (total - (3600 * h)) / 60;
            int s = (total % 60);
            Console.WriteLine($"Heure: {h} , Minutes: {m},  Secondes: {s}");
            Console.ReadKey();
        }
    }
}
