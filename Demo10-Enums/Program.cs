﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo10_Enums
{
    class Program
    {
        public enum JourSemaine
        {
            Lundi = 1, 
            Mardi = 2, 
            Mercredi = 3, 
            Jeudi = 4, 
            Vendredi = 5, 
            Samedi = 6, 
            Dimanche = 7
        }
        static void Main(string[] args)
        {
            // Changer la couleur de la police dans la console
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("test");

            Console.WriteLine(IsWeekDay(JourSemaine.Lundi));

            

            Console.ReadKey();
        }

        public static bool IsWeekDay(JourSemaine jour)
        {

            if ((int)jour >= 6)
                return false;
            else return true;
        }
    }
}
