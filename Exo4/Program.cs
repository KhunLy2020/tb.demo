﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(SquareRoots(2));
            // Bonus: Sauvegarde dans un fichier 
            // Créer un dictionnaire vide //bar <nom, qt>
            Dictionary<string, int> bar = new Dictionary<string, int>();
            // dans une boucle
            while (true)
            {
                AfficherMonMenu();
                int choix = GetEntier("Entrer votre choix", "T'es con ou quoi?");
                if (choix == 1)
                    // creer une fonction Ajouter()
                    Ajouter(bar);
                else if(choix ==2)
                    // Modifier()
                    Modifier(bar); 
                else if(choix == 3)
                    //Supprimer()
                    Supprimer(bar);
                // afficher l'état du stock
                Afficher(bar);
            }
        }

        public static void Supprimer(Dictionary<string, int> bar)
        {
            //demander le produit à supprimer
            Console.WriteLine("Entrer le nom ...");
            string nom = Console.ReadLine().ToUpper();
            if (bar.ContainsKey(nom))
            {
                //supprimer du dico
                bar.Remove(nom);
            }
            else
            {
                Console.WriteLine("Le produit que vous voulez supprimer n'existe pas");
            }
        }

        private static void Afficher(Dictionary<string, int> bar)
        {
            Console.Clear();
            foreach (KeyValuePair<string, int> kvp in bar)
            {
                Console.WriteLine($"{kvp.Key}: {kvp.Value}");
            }
        }


        public static void Modifier(Dictionary<string, int> bar)
        {
            Console.WriteLine("Entrer le nom du produit à modifier");
            string nom = Console.ReadLine();
            nom = nom.ToUpper();
            if (bar.ContainsKey(nom))
            {
                bool fini = false;
                // Afficher La quantité
                while (!fini)
                {
                    Console.Clear();
                    Console.WriteLine("Quantité: " + bar[nom]);
                    ConsoleKey key = Console.ReadKey().Key;
                    if (key == ConsoleKey.Enter)
                    {
                        fini = true;
                    }
                    else if (key == ConsoleKey.DownArrow)
                    {
                        if (bar[nom] != 0)
                        {
                            bar[nom]--;
                        }
                    }
                    else if (key == ConsoleKey.UpArrow)
                    {
                        bar[nom]++;
                    }
                }
                // modifier dans le dico
            }
            else
            {
                Console.WriteLine("Le Produit n'existe pas");
            }
        }

        public static void Ajouter(Dictionary<string, int> bar)
        {
            // demander à l'utilisateur le nom de la clé à ajouter dans ce dictionnaire
            Console.WriteLine("Quel Produit souhaitez-vous ajouter ?");
            // demander à l'utilisateur de spcécifier la quantité
            string nom = Console.ReadLine();
            // changer les lettres en maj
            nom = nom.ToUpper();
            // changer les lettres en min
            // nom = nom.ToLower();
            while (bar.ContainsKey(nom))
            {
                Console.WriteLine("Le produit existe déjà ?");
                // demander à l'utilisateur de spcécifier la quantité
                nom = Console.ReadLine();
            }
            int nb = GetEntier("Entrer la quantité souhaitée");
            // Ajouter les infos dans le dico
            bar.Add(nom, nb);
        }

        public static void AfficherMonMenu()
        {
            Console.WriteLine("1.Ajouter");
            Console.WriteLine("2.Modifier");
            Console.WriteLine("3.Supprimer");
            Console.WriteLine("4.Sauvegarder");
        }

        public static int GetEntier(string message, string errorMessage = "Valeur incorrecte")
        {
            Console.WriteLine(message);
            string entree = Console.ReadLine();
            int nb;
            while(!int.TryParse(entree, out nb))
            {
                Console.WriteLine(errorMessage);
                entree = Console.ReadLine();
            }
            return nb;
        }

        private static double SquareRoots(int v)
        {
            double root = 1;
            int i = 0;

            while (true)
            {
                i = i + 1;
                root = (v / root + root) / 2;
                if (i == v + 1) { break; }
            }
            return root;
        }
    }
}
