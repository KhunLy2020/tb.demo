﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            //numeriques
            int nb1 = 300000000;
            long nb4 = 30000000000;
            float nb2 = 3.14F;
            double nb3 = 3.14;


            //caractères
            char c1 = 'a'; // entre simple quote
            string s1 = "ma chaine de caractères"; // entre double quote

            //boolean
            bool b1 = true;

            //date
            DateTime date = DateTime.Now;
            DateTime date2 = new DateTime(1982, 5, 6);

            //object
            object o1 = new object();

            //tableau
            int[] tab1 = new int[2] { 1,2 };

            // Object - ref

            int[] tab2 = tab1;
            tab1[0] = 42;

            Console.WriteLine("tab2[0] : " + tab2[0]);


            // struct - valeur

            int nb42 = nb1;
            nb1 = 42;
            Console.WriteLine("nb42 " + nb42);

            //exception string
            string s2 = s1;
            s1 = "ma nouvelle valeur";
            Console.WriteLine(s2);

            // type cts vs type .net
            int nb43;
            Int32 nb44;

            string s42;
            String s43;


            //Sortie console cw tab 2*
            Console.WriteLine("sortie console");

            // Entrée user
            string entree = Console.ReadLine();

            Console.WriteLine(entree);

            Console.WriteLine("nb1 " + nb1);
            Console.WriteLine("Hello Khun !!!");
            Console.ReadKey();
        }
    }
}
