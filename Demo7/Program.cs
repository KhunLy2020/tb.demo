﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Demo7
{
    class Program
    {
            
        public static Random generator = new Random();
        static void Main(string[] args)
        {
            #region Boucles
            //for (int i = 0; i < 10; i++)
            //{
            //    // Na pas toucher l'itérateur
            //    //i--;
            //    Console.WriteLine("X");
            //}

            //List<string> liste = new List<string>() { "Riri", "Fifi", "Loulou" };
            //foreach(string item in liste)
            //{
            //    Console.WriteLine(item);

            //    // déconseillé
            //    //if (item == "Fifi") break;
            //} 
            #endregion

            #region Délai + Aléatoire
            //while (true)
            ////for(int i = 0; i< 10; i++)
            //{
            //    int nb = generator.Next(10, 20);
            //    Console.WriteLine(nb);
            //    // défini un délai en millisecondes 
            //    // nécessite le using System.Threading;
            //    // alt + enter pour ajouter l'import
            //    Thread.Sleep(1000);
            //} 
            #endregion
            Console.ReadKey();
        }
    }
}
